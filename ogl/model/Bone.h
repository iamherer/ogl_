#pragma once
#include "ogl/model/pch.h"
#include "ogl/model/VertexWeight.h"

namespace Ogl
{
    namespace Model
    {
        struct Bone
        {
            std::string m_Name;
            unsigned int m_Index;
            std::vector<VertexWeight> m_Weights;
            glm::mat4 m_OffsetMatrix;

            Bone(aiBone *bone);
        };
    }
}