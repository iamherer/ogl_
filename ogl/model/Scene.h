#pragma once

#include "ogl/model/pch.h"
#include "ogl/model/Mesh.h"
#include "ogl/model/Material.h"
#include "ogl/model/Animation.h"
#include "ogl/model/Node.h"
#include "ogl/model/Camera.h"
#include "ogl/model/Light.h"

namespace Ogl
{
    namespace Model
    {
        struct Scene
        {

            std::string m_Name;
            std::string m_Path;
            
            Ogl::Model::AABB m_AABB;
            std::shared_ptr<Node> m_RootNode;
            std::vector<std::shared_ptr<Ogl::Model::Mesh>> m_Meshes;
            std::vector<std::shared_ptr<Ogl::Model::Animation>> m_Animations;
            std::vector<std::shared_ptr<Ogl::Model::Material>> m_Materials;
            std::vector<std::shared_ptr<Ogl::Model::Camera>> m_Cameras;
            std::vector<std::shared_ptr<Ogl::Model::Light>> m_Lights;

            std::shared_ptr<Ogl::Gut::RenderTarget> m_MeshesRenderTarget;

            Scene() = default;

            Scene(const std::string &path);

            bool Create(const std::string &path);

            bool Create(const aiScene *source, const std::string &path);

            bool CreateSnapshot();

            void CalAABB(const Ogl::Model::AABB& other);
        };
    };
};
