#pragma once

#include "ogl/model/pch.h"
#include "ogl/gut/Texture2D.h"
#include "ogl/model/Property.h"

#include <map>
#include <vector>

namespace Ogl
{
    namespace Model
    {
        struct Material
        {
            std::string m_Name;

            std::string m_Dir;

            std::map<std::string, std::vector<Ref<Ogl::Gut::Texture2D>>> m_Textures;

            std::map<std::string, Ref<Ogl::Gut::Texture2D>> m_TextureMap;

            Material(aiMaterial *mat, const std::string &dir);

            void LoadTextures(aiMaterial *mat ,aiTextureType type, const std::string &info);

            std::vector<Ref<Property>> m_Properties;

            bool HasTexture(const std::string& type);
        };
    };
};