#pragma once
#include "ogl/model/pch.h"

namespace Ogl
{
    namespace Model
    {
        struct Light
        {
            std::string m_Name;

            enum LightSourceType
            {
                _UNDEFINED = 0x0,
                _DIRECTIONAL = 0x1,
                _POINT = 0x2,
                _SPOT = 0x3,
                _AMBIENT = 0x4,
                _AREA = 0x5,
            } m_LightType;

            static std::vector<std::string> g_sourceTypesStrs;


            glm::vec3 m_Position;
            glm::vec3 m_Direction;
            glm::vec3 m_Up;
            float m_AttenuationConstant;
            float m_AttenuationLinear;
            float m_AttenuationQuadratic;

            glm::vec3 m_ColorDiffuse;
            glm::vec3 m_ColorSpecular;
            glm::vec3 m_ColorAmbient;

            float m_AngleInnerCone;
            float m_AngleOuterCone;

            glm::vec2 m_Size;

            Light(aiLight *light);
        };
    }
}