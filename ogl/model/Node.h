#pragma once

#include "ogl/model/pch.h"

namespace Ogl
{
    namespace Model
    {
        struct Node
        {
            std::string m_Name;
            std::vector<Ref<Node>> m_Childern;
            std::vector<unsigned int> m_Meshes;
            glm::mat4 m_Offset;
            
            Node(aiNode *node);
        };
    }
}