#pragma once

#include "ogl/model/Scene.h"
#include "ogl/gut/Effect3d.h"


namespace Ogl
{
	namespace Model
	{
		struct BoneNode
		{
			std::string m_NodeName;
			glm::mat4 m_NodeOffset;
			glm::mat4 m_NodeTransfrom;
			glm::mat4 m_BoneOffset;
			glm::mat4 m_BoneFinal;
			int m_BoneIndex = -1;

			BoneNode(const Ogl::Model::Node* node, const std::map<std::string, std::shared_ptr<Ogl::Model::Bone>>& bones);

			void ProcessNode(glm::mat4 parentModel);
			void UpdateBuffer(Ogl::Gut::Effect3d::BoneData& target);

			std::vector<std::shared_ptr<BoneNode>> children;
		};

	};
};
