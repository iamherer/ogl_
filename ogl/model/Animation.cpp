#include "ogl/model/Animation.h"


Ogl::Model::Animation::Animation(aiAnimation* anim)
{
    Load(anim->mName, m_Name);
    m_Duration = anim->mDuration;
    m_TicksPerSecond = anim->mTicksPerSecond;
    m_TotalTime = m_Duration / m_TicksPerSecond;

    for (int i = 0; i < anim->mNumChannels; i++)
    {
        std::shared_ptr<Ogl::Model::Channel> newChannel = std::make_shared<Ogl::Model::Channel>(anim->mChannels[i]);
        m_Channels[newChannel->m_NodeName] = newChannel;

    }
};