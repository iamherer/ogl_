#pragma once

#include "ogl/model/pch.h"

namespace Ogl
{
    namespace Model
    {
        struct RotationKey
        {
            double m_Time;

            glm::quat m_Value;

            RotationKey(aiQuatKey *key)
            {
                m_Time = key->mTime;
                Load(key->mValue, m_Value);
            }
        };
    }
}