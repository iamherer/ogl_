#pragma once

#include "ogl/model/pch.h"

namespace Ogl
{
    namespace Model
    {
        struct VertexWeight
        {
            unsigned int m_VertexId;
            float m_Weight;

            VertexWeight(aiVertexWeight *vw)
                : m_VertexId(vw->mVertexId),
                  m_Weight(vw->mWeight)
            {
            }
        };
    }
}