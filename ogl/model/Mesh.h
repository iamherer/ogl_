#pragma once

#include "ogl/gut/types.h"
#include "ogl/model/pch.h"
#include "ogl/model/Face.h"
#include "ogl/model/AABB.h"
#include "ogl/model/Bone.h"
#include "ogl/model/Shader.h"
#include "ogl/model/Node.h"
#include "ogl/phong/Effect.h"
#include "ogl/math/Camera.h"
#include "ogl/math/SkinnedMeshData.h"
#include "ogl/math/GeometryData.h"

namespace Ogl
{
    namespace Model
    {
        struct Mesh
        {

            std::string m_Name;

            int m_MaterialIndex = -1;

            Ogl::Model::AABB m_AABB;

            std::map<std::string, Ref<Bone>> m_Bones;

            std::shared_ptr<Ogl::Gut::Mesh> m_Mesh;

            Ref<Ogl::Gut::RenderTarget> m_MeshRT, m_SkinnedMeshRT;

            Mesh(aiMesh* mesh);

            bool CreateRenderMesh(aiMesh* mesh);

            bool CreateSnapshot(const Ogl::Model::Node* rootNode);
           
            bool HasBones() const;

        
        };
    };
};