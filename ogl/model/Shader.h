#pragma once

#include "ogl/gut/Shader.h"

namespace Ogl
{
	namespace Model
	{
		struct Shader :Ogl::Gut::Shader
		{
			Shader();
		};
	};
};