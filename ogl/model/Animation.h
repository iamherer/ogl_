#pragma once
#include "ogl/model/pch.h"
#include "ogl/model/Channel.h"

namespace Ogl
{
    namespace Model
    {
        struct Animation
        {
            std::string m_Name;

            double m_Duration;

            double m_TicksPerSecond;

            double m_TotalTime;

            std::map<std::string ,Ref<Ogl::Model::Channel>> m_Channels;

            Animation(aiAnimation* anim);


        };
    }
}