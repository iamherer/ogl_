#include "ogl/model/Material.h"

Ogl::Model::Material::Material(aiMaterial *mat, const std::string &dir)
    : m_Dir(dir)
{
    // wtclog::info("load Material");
    Load(mat->GetName(), m_Name);

    LoadTextures(mat, aiTextureType_NONE, "NONE");
    LoadTextures(mat, aiTextureType_DIFFUSE, "DIFFUSE");
    LoadTextures(mat, aiTextureType_SPECULAR, "SPECULAR");
    LoadTextures(mat, aiTextureType_AMBIENT, "AMBIENT");
    LoadTextures(mat, aiTextureType_EMISSIVE, "EMISSIVE");
    LoadTextures(mat, aiTextureType_HEIGHT, "HEIGHT");
    LoadTextures(mat, aiTextureType_NORMALS, "NORMALS");
    LoadTextures(mat, aiTextureType_SHININESS, "SHININESS");
    LoadTextures(mat, aiTextureType_OPACITY, "OPACITY");
    LoadTextures(mat, aiTextureType_DISPLACEMENT, "DISPLACEMENT");
    LoadTextures(mat, aiTextureType_LIGHTMAP, "LIGHTMAP");
    LoadTextures(mat, aiTextureType_REFLECTION, "REFLECTION");
    LoadTextures(mat, aiTextureType_BASE_COLOR, "BASE_COLOR");
    LoadTextures(mat, aiTextureType_NORMAL_CAMERA, "NORMAL_CAMERA");
    LoadTextures(mat, aiTextureType_EMISSION_COLOR, "EMISSION_COLOR");
    LoadTextures(mat, aiTextureType_METALNESS, "METALNESS");
    LoadTextures(mat, aiTextureType_DIFFUSE_ROUGHNESS, "DIFFUSE_ROUGHNESS");
    LoadTextures(mat, aiTextureType_AMBIENT_OCCLUSION, "AMBIENT_OCCLUSION");
    LoadTextures(mat, aiTextureType_UNKNOWN, "UNKNOWN");

    for (unsigned int i = 0; i < mat->mNumProperties;i++)
    {
        m_Properties.push_back(MakeRef<Ogl::Model::Property>(mat->mProperties[i]));
    }
}

void Ogl::Model::Material::LoadTextures(aiMaterial *mat, aiTextureType type, const std::string &info)
{
    unsigned int num = mat->GetTextureCount(type);

    if (num > 0)
    {
        for (int i = 0; i < num; i++)
        {
            aiString str;
            mat->GetTexture(type, i, &str);

            std::string fileName;
            Load(str, fileName);

            // std::cout << info << "texture Path" << info << std::endl;
            std::string texturePath = m_Dir + "/" + fileName;
            auto &textures = m_Textures[info];

            wtclog::info("%s %d : %s \n", info.c_str(), i, texturePath.c_str());

            if (m_TextureMap.find(texturePath) == m_TextureMap.end())
            {

                Ref<Ogl::Gut::Texture2D> newTexture = MakeRef<Ogl::Gut::Texture2D>(texturePath);

                if (newTexture->m_Image->Avail())
                {
                    textures.push_back(newTexture);
                    m_TextureMap[texturePath] = newTexture;
                }
                else
                {
                    wtclog::info("cannot load %s \n", texturePath.c_str());
                }
            }else
            {
                textures.push_back(m_TextureMap[texturePath]);
            }

        }
    }
}
bool Ogl::Model::Material::HasTexture(const std::string& type)
{

    auto it = m_Textures.find(type);

    return it!=m_Textures.end();
}
;
