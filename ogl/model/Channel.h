#pragma once

#include "ogl/model/pch.h"
#include "ogl/model/PositionKey.h"
#include "ogl/model/RotationKey.h"
#include "ogl/model/ScaleKey.h"

#include "ogl/math/Transform.h"

namespace Ogl
{
    namespace Model
    {
        struct Channel
        {
            std::string m_NodeName;

            std::vector<Ref<Ogl::Model::PositionKey>> m_PositionKeys;
            std::vector<Ref<Ogl::Model::RotationKey>> m_RotationKeys;
            std::vector<Ref<Ogl::Model::ScaleKey>> m_ScaleKeys;

            Channel(aiNodeAnim* channel);

            glm::vec3 GetPosition(double ct_time) const;
            glm::quat GetRotation(double ct_time) const;
            glm::vec3 GetScale(double ct_time) const;

            int GetPositionIndex(double index) const;
            int GetRotationIndex(double index) const;
            int GetScaleIndex(double index) const;

            glm::mat4 GetSlerpTransform(double index) const;
        };
    }
};