#pragma once



#include "ogl/gut/pch.h"

namespace Ogl {
    namespace Pbrx {


        struct PointLight
        {

            glm::vec3 pos;
            float pad;
            glm::vec3 color;
            float pad2;

        };
        struct LightEffect {


            PointLight lights[4];
            LightEffect()
            {
                lights[0].pos = glm::vec3(-10.0f, 10.0f, 10.0f);
                lights[1].pos = glm::vec3(10.0f, 10.0f, 10.0f);
                lights[2].pos = glm::vec3(-10.0f, -10.0f, 10.0f);
                lights[3].pos = glm::vec3(10.0f, -10.0f, 10.0f);
                for (int i = 0; i < 4; i++)
                {
                    lights[i].color = glm::vec3(300.0f, 300.0f, 300.0f);
                }
            }
        };
    }
}