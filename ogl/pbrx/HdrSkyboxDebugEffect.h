#pragma once


#include "ogl/gut/SkyboxEffect.h"

struct HdrSkyboxDebugEffect :public Ogl::Gut::SkyboxEffect
{

	HdrSkyboxDebugEffect();
	void Begin();
	void RenderSkybox(const Ogl::Gut::TextureCube* texture);
	void End();
	static std::string FragCode();
};