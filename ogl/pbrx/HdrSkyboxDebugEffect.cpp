#include "HdrSkyboxDebugEffect.h"

HdrSkyboxDebugEffect::HdrSkyboxDebugEffect()
    :Ogl::Gut::SkyboxEffect(HdrSkyboxDebugEffect::FragCode())
{

}

void HdrSkyboxDebugEffect::Begin()
{
    Ogl::Gut::SkyboxEffect::Begin();
    m_Shader->Use();
    m_Shader->setInt("environmentMap", 0);

}

void HdrSkyboxDebugEffect::RenderSkybox(const Ogl::Gut::TextureCube* texture)
{
    m_Shader->Use();
    m_Shader->setInt("environmentMap", 0);
    texture->Active(0);
    m_SkyboxMesh->Bind();
    m_SkyboxMesh->DrawElementsAuto();
}

void HdrSkyboxDebugEffect::End()
{
    Ogl::Gut::SkyboxEffect::End();
}

std::string HdrSkyboxDebugEffect::FragCode()
{
	return R"(
out vec4 FragColor;
in vec3 localpos;

uniform samplerCube environmentMap;

void main()
{		
    vec3 envColor = textureLod(environmentMap, localPos, 0.0).rgb;
    
    // HDR tonemap and gamma correct
    envColor = envColor / (envColor + vec3(1.0));
    envColor = pow(envColor, vec3(1.0/2.2)); 
    
    FragColor = vec4(envColor, 1.0);
}
)";
}
