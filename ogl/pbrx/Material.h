#pragma once

#include "ogl/gut/pch.h"

namespace Ogl
{
	namespace Pbrx
	{

		struct Material
		{
			glm::vec3 albedo;
			float metallic;
			float roughness;
			float ao;
			float pad[2];
		};
	}
}
