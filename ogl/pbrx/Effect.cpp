#include "ogl/pbrx/Effect.h"

using namespace Ogl::Pbrx;

Effect::Effect()
    :Ogl::Gut::Effect3d(Ogl::Pbrx::Effect::FragCode())
{
    Init();

}

void Effect::Begin()
{
    Ogl::Gut::Effect3d::Begin();

    m_LightBuffer->Binding(3);
    m_MatBuffer->Binding(4);

    m_Shader->Use();
    m_Shader->Bind("LightBuffer", 3);
    m_Shader->Bind("MatBuffer", 4);
    m_Shader->setInt("irradianceMap", 0);
    m_Shader->setInt("prefilterMap", 1);
    m_Shader->setInt("brdfLUT", 2);

    m_AnimShader->Use();
    m_AnimShader->Bind("LightBuffer", 3);
    m_AnimShader->Bind("MatBuffer", 4);
    m_AnimShader->setInt("irradianceMap", 0);
    m_AnimShader->setInt("prefilterMap", 1);
    m_AnimShader->setInt("brdfLUT", 2);
}

void Ogl::Pbrx::Effect::Init()
{
    m_MatBuffer = std::make_shared<Ogl::Gut::UniformBuffer>(sizeof(Material));
    m_LightBuffer = std::make_shared<Ogl::Gut::UniformBuffer>(sizeof(LightEffect));
}


void Effect::RenderMesh(const Ogl::Gut::Mesh* mesh, const Material& material, const Ogl::Math::Transform& transform)
{
    m_MatBuffer->Upload<Material>(material);
    Ogl::Gut::Effect3d::RenderMesh(mesh, transform);

}
void Effect::End()
{
    Ogl::Gut::Effect3d::End();
}

void Effect::SetHdrTexture(const HdrTexture* hdrTexture)
{
    hdrTexture->Active();
}

void Ogl::Pbrx::Effect::SetLightEffect(const LightEffect& inst)
{
    m_LightBuffer->Upload<LightEffect>(inst);
}