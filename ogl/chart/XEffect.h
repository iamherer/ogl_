#pragma once

#include "ogl/gut/EffectBase.h"
#include "ogl/gut/Shader.h"
#include "ogl/gut/Mesh.h"
#include "ogl/font/FontLib.h"

#include "ogl/chart/LineChart.h"

namespace Ogl
{
	namespace Chart
	{

		struct XEffect :public Ogl::Gut::EffectBase
		{
			std::shared_ptr<Ogl::Gut::Shader> m_Shader;

			int m_ScrWidth, m_ScrHeight;

			XEffect();
			void Begin();
			void RenderChart(const LineChart* inst);
			void End();

		};
	};
};