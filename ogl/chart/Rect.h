#pragma once


#include "ogl/gut/pch.h"

namespace Ogl
{
    namespace Chart
    {

        struct Rect
        {
            glm::vec2 pos, size;
            void Viewport(int width, int height)
            {
                glViewport(pos.x * width, pos.y * height, size.x * width, size.y * height);
            }
            glm::vec2 BottomLeftFloat()
            {
                glm::vec2 res = pos;
                res.y += size.y;
                return res;
            }
        };
    };
};
