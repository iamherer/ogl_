#pragma once

#include "ogl/gut/pch.h"

namespace Ogl
{
	namespace Chart
	{
		struct AABB
		{
			glm::vec2 m_Min, m_Max;

			float DeltaX()
			{
				return m_Max.x - m_Min.y;
			}
			float DeltaY()
			{
				return m_Max.y - m_Min.y;
			}

			static float CalculateStep(float value) {
				double magnitude = std::pow(10, std::floor(std::log10(value)));

				if (value / magnitude >= 8.0) {
					return magnitude * 0.1;  
				}
				else {
					return magnitude;
				}
			}

			glm::mat4 GetProj() const
			{
				return glm::ortho(m_Min.x, m_Max.x, m_Min.y, m_Max.y);
			}
			AABB()
				:m_Min(glm::vec2(0.0f)), m_Max(glm::vec2(0.0f))
			{

			}
			glm::vec2 Center() const
			{
				return (m_Max + m_Min) / 2.0f;

			}
			glm::vec2 HalfExtends() const
			{
				return (m_Max - m_Min) / 2.0f;
			}
		};
	};
};