#include "XEffect.h"

Ogl::Chart::XEffect::XEffect() :Ogl::Gut::EffectBase()
{

	std::string vertCode = R"(

#version 330 core

layout(location=0) in vec3 aPos;

uniform mat4 proj;

void main()
{
   gl_Position= proj* vec4(aPos,1.0f);
};
)";

	std::string fragCode = R"(

#version 330 core

out vec4 FragColor;

uniform vec4 lineColor;

void main()
{
    FragColor=lineColor;
};

)";
	m_Shader = std::make_shared<Ogl::Gut::Shader>(vertCode.c_str(), fragCode.c_str());

}

void Ogl::Chart::XEffect::Begin()
{
	m_Shader->Use();
}


void Ogl::Chart::XEffect::RenderChart(const LineChart* inst)
{

	m_Shader->Use();
	m_Shader->setMat4("proj", inst->m_RenderAABB.GetProj());
	m_Shader->setVec4("lineColor", glm::vec4(inst->m_Color, 1.0));

	inst->m_PointBuffer->Bind();
	inst->m_PointBuffer->DrawElementsAuto();
}

void Ogl::Chart::XEffect::End()
{
	Ogl::Gut::Shader::UnUse();

}
