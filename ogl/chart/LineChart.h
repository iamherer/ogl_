#pragma once

#include "ogl/gut/Mesh.h"
#include "ogl/font/FontLib.h"
#include "ogl/chart/AABB.h"
#include "ogl/chart/Rect.h"
#include "ogl/font/TextLine.h"

struct LineChart {

	std::shared_ptr<Ogl::Gut::Mesh> m_PointBuffer;
	LineChart(const std::vector<glm::vec2>& sourcePos) :m_Color(glm::vec3(0.0f))
	{
		// CreateMesh Buffer
		CreatePointMesh(sourcePos);
		// Cal X Y mark point
		CalXYMark();
	}
	void CalXYMark();
	void CreatePointMesh(const std::vector<glm::vec2>& sourcePos);
	void CalAABB(const glm::vec2& pos);
	Ogl::Chart::AABB m_AABB;
	Ogl::Chart::AABB m_RenderAABB;
	void SetRenderAABB(const glm::vec2& scale);

	// Line Color
	glm::vec3 m_Color;

	// TextLines
	Ogl::Font::TextLine m_Title;
	
	// Mark the X and the Y
	std::vector<Ogl::Font::TextLine> m_XAxisTexts, m_YAxisTexts;

};
