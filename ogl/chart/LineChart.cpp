#include "ogl/chart/LineChart.h"

void LineChart::CalXYMark()
{
	{
		Ogl::Font::TextLine textLine;
		textLine.m_FontSize = 17;
		textLine.m_Pos = glm::vec2(0.f);
		textLine.m_Text = std::to_string(m_AABB.m_Min.x);
		m_XAxisTexts.push_back(textLine);
	}

	{
		Ogl::Font::TextLine textLine;
		textLine.m_FontSize = 17;
		textLine.m_Pos = glm::vec2(0.f);
		textLine.m_Text = std::to_string(m_AABB.m_Min.y);
		m_YAxisTexts.push_back(textLine);
	}


}

void LineChart::CreatePointMesh(const std::vector<glm::vec2>& sourcePos)
{
	m_PointBuffer = std::make_shared<Ogl::Gut::Mesh>();

	m_PointBuffer->Bind();

	std::vector<unsigned int> indices;
	std::vector<glm::vec3> positions;
	for (int i = 0; i < sourcePos.size(); i++)
	{
		const auto& pos = sourcePos[i];
		positions.push_back(glm::vec3(pos.x, pos.y, 0.0f));

		indices.push_back(i);
		CalAABB(pos);

	}

	SetRenderAABB(glm::vec2(1.0f, 2.0f));;

	m_PointBuffer->PushVertexBuffer<glm::vec3>(positions);
	m_PointBuffer->RegisterIndexBuffer<unsigned int>(indices);
	m_PointBuffer->m_Desc.faceType = GL_LINE_STRIP;

	Ogl::Gut::Mesh::UnBind();
}

void LineChart::CalAABB(const glm::vec2& pos)
{
	if (m_AABB.m_Min.x > pos.x)
	{
		m_AABB.m_Min.x = pos.x;
	}
	if (m_AABB.m_Min.y > pos.y)
	{
		m_AABB.m_Min.y = pos.y;
	}

	if (m_AABB.m_Max.x < pos.x)
	{
		m_AABB.m_Max.x = pos.x;
	}
	if (m_AABB.m_Max.y < pos.y)
	{
		m_AABB.m_Max.y = pos.y;
	}
}

void LineChart::SetRenderAABB(const glm::vec2& scale)
{
	glm::vec2 center = m_AABB.Center();
	glm::vec2 halfExt = m_AABB.HalfExtends();

	m_RenderAABB.m_Max.y = center.y + scale.y * halfExt.y;
	m_RenderAABB.m_Min.y = center.y - scale.y * halfExt.y;

	m_RenderAABB.m_Max.x = center.x + scale.x * halfExt.x;
	m_RenderAABB.m_Min.x = center.x - scale.x * halfExt.x;

}