#include "ogl/debug/Textured2dArrayEffect.h"

using namespace Ogl::_debug;

Texture2dArrayEffect::Texture2dArrayEffect()
	:Ogl::Gut::Effect2d(Texture2dArrayEffect::GetFragCode())
{
}

void Ogl::_debug::Texture2dArrayEffect::Begin()
{
	m_Shader->Use();
	m_Shader->setInt("texArray", 0);
	//m_Shader->setInt("texIndex", m_DepthIndex);
	Ogl::Gut::Effect2d::Begin();

}

void Ogl::_debug::Texture2dArrayEffect::RenderTexture(const Ogl::Gut::Texture2DArray* tex, const int depthIndex)
{

	m_Shader->Use();
	m_Shader->setInt("texIndex", depthIndex);
	tex->Active(0);
	m_2DShowMesh->Bind();
	m_2DShowMesh->DrawElementsAuto();

}

std::string Texture2dArrayEffect::GetFragCode()
{
	const std::string fragCode = R"(

out vec4 FragColor;
uniform sampler2DArray texArray;
uniform int texIndex;

void main()
{
   FragColor=texture(texArray, vec3(TexCoord, texIndex));   
};

)";

	return fragCode;
}
