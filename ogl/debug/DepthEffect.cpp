#include "ogl/debug/DepthEffect.h"

using namespace Ogl::_debug;

DepthEffect::DepthEffect()
	:Ogl::Gut::Effect2d(DepthEffect::GetFragMain())
{
	
}

void DepthEffect::Begin()
{
	m_Shader->Use();
}

void DepthEffect::End()
{
	Ogl::Gut::Shader::UnUse();
};

void DepthEffect::RenderTexture(Ogl::Gut::Mesh* mesh2d, const Ogl::Gut::Texture2D* texture)
{
	m_Shader->Use();
	m_Shader->setInt("depthMap", 0);
	texture->Active(0);
	mesh2d->DrawElementsAuto();
}

const std::string DepthEffect::GetFragMain()
{
	std::string fragMain = R"(


out vec4 FragColor;

uniform sampler2D depthMap;

void main()
{
	float depth = texture(depthMap, TexCoord).r;	
	FragColor=vec4(vec3(depth),1.0f);
};
)";
	return fragMain;
}

