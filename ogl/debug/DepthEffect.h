#pragma once

#include "ogl/gut/types.h"

namespace Ogl
{
	namespace _debug
	{

		struct DepthEffect :public Ogl::Gut::Effect2d
		{


			DepthEffect();

			void Begin();

			void RenderTexture(Ogl::Gut::Mesh* mesh2d, const Ogl::Gut::Texture2D* texture);
			void End();

			static const std::string GetFragMain();
		};
	};
};