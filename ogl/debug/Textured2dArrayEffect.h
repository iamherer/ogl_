#pragma once

#include "ogl/gut/pch.h"
#include "ogl/gut/Effect2d.h"
#include "ogl/gut/Texture2DArray.h"

namespace Ogl
{
	namespace _debug
	{

		struct Texture2dArrayEffect :public Ogl::Gut::Effect2d
		{

			Texture2dArrayEffect();
			void Begin();
			void RenderTexture(const Ogl::Gut::Texture2DArray* tex, const int depthIndex);
			static std::string GetFragCode();
		};
	}
};