#pragma once

#include "ogl/gut/UniformBuffer.h"
#include "ogl/gut/Shader.h"
#include "ogl/gut/Mesh.h"
#include "ogl/math/Transform.h"
#include "ogl/math/Geometry.h"
#include "ogl/gut/EffectBase.h"

#include "mustache.hpp"


namespace Ogl
{
	namespace Gut
	{
		struct Effect2d :public Ogl::Gut::EffectBase
		{

			std::shared_ptr<Ogl::Gut::Shader>  m_Shader;
			std::shared_ptr<Ogl::Gut::Mesh> m_QuadMesh, m_2DShowMesh;

			Effect2d(const std::string& fCode);
			std::string m_FragMain;

			void Begin();

			void RenderMesh(const Ogl::Gut::Mesh* mesh);
			void DebugQuad();
			void Debug2DShowMesh();
			void End();

			std::string GetFragCode();
			static std::string GetVertCode();
			static std::string GetFragTemp();
		};
	};
};