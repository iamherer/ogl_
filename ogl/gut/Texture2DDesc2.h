#pragma once


#include "ogl/gut/Type.h"
#include "ogl/gut/Image.h"
#include "ogl/gut/Base.h"

namespace Ogl
{
	namespace Gut
	{
		struct Texture2DDesc2 :public Base
		{
		
			int m_Mipmap;
			Ogl::Gut::SamplerType m_SamplerType;
			Ogl::Gut::Image::Desc m_ImageDesc;
			Texture2DDesc2()
				:m_Mipmap(0), m_SamplerType(SamplerType::_sampler_none_)
			{

			}
		};
	};
};