#pragma once


#include <iostream>

namespace Ogl
{
	namespace Gut
	{
		struct MeshOffset {

			unsigned int numIndices;
			unsigned int indexOffset;
		};
	};
};
