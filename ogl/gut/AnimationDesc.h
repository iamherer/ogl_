#pragma once

#include "ogl/gut/pch.h"
#include "ogl/gut/Type.h"
#include "ogl/gut/AnimationDesc1.h"
#include "ogl/gut/Base.h"

namespace Ogl
{
	namespace Gut
	{

		struct AnimationDesc :public Base{
		
			AnimationDesc1 m_Desc1;
			AnimationDescType m_Type;
			
			AnimationDesc() 
				:m_Type(AnimationDescType::_None_)
			{

			}

			AnimationDesc(const AnimationDesc1& d1) 
				: m_Desc1(d1), m_Type(AnimationDescType::_Animation_Model_File_) 
			{
			};

		};
	};
};