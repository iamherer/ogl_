#pragma once

#include "ogl/gut/pch.h"

namespace Ogl
{
    namespace Gut
    {
        struct RotationKey
        {
            double m_Time;

            glm::quat m_Value;

            RotationKey(aiQuatKey *key)
            {
                m_Time = key->mTime;
                Load(key->mValue, m_Value);
            }
        };
    }
}