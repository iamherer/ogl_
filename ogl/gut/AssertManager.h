#pragma once

#include "ogl/gut/pch.h"
#include "ogl/gut/Scene.h"

namespace Ogl
{
    namespace Gut
    {
        struct AssertManager
        {
            std::unordered_map<std::string, std::shared_ptr<Ogl::Gut::Scene>> m_Scenes;

            std::unordered_map<std::string, std::string> m_Texts;

            std::string GetText(const std::string& path);

            std::shared_ptr<Ogl::Gut::Scene> GetScene(const std::string &path);

            std::shared_ptr<Ogl::Gut::Channel> GetNodeAnim(const Ogl::Gut::Channel::Desc &desc);

            std::shared_ptr<Ogl::Gut::Animation> GetAnimation(const AnimationDesc &desc);

            std::shared_ptr<Ogl::Gut::Animation> GetAnimation(const AnimationDesc1 &desc);

            std::shared_ptr<Ogl::Gut::Animation> GetAnimation(const std::string& path, int index);

            std::shared_ptr<Ogl::Gut::MeshX> GetMeshX(const std::string& path, int index);

            std::shared_ptr<Ogl::Gut::Mesh> GetMesh(const Ogl::Gut::MeshDesc& meshDesc);

            std::shared_ptr<Ogl::Gut::Material> GetMaterial(const Ogl::Gut::Material::Desc &desc);

            bool LoadScene(const std::string& path);

            static AssertManager *Inst();
        };
    };
};
