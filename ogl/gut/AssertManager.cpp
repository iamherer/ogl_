#include "ogl/gut/AssertManager.h"
#include "AssertManager.h"

using namespace Ogl::Gut;


bool Ogl::Gut::AssertManager::LoadScene(const std::string &path) 
{
    std::shared_ptr<Ogl::Gut::Scene> scene = std::make_shared<Ogl::Gut::Scene>(Ogl::Gut::Scene(path));
    if(scene->Avail())
    {
        m_Scenes[path] = scene;
    }
    return scene->Avail();
}

std::shared_ptr<Ogl::Gut::Animation> Ogl::Gut::AssertManager::GetAnimation(const std::string& path, int index)
{
    auto scene = GetScene(path);

    if (scene)
    {
        if (scene->m_Animations.size() > index)
        {
            return scene->m_Animations[index];
        }
    }
    return 0;
}

std::shared_ptr<Ogl::Gut::MeshX> Ogl::Gut::AssertManager::GetMeshX(const std::string& path, int index)
{
    auto scene = GetScene(path);

    if (scene)
    {
        if (scene->m_Meshes.size() > index)
        {
            return scene->m_Meshes[index];
        }
    }
    return 0;
}

std::shared_ptr<Ogl::Gut::Mesh> Ogl::Gut::AssertManager::GetMesh(const Ogl::Gut::MeshDesc& meshDesc)
{
    if (meshDesc.m_Type == MeshDescType::_mesh_model_file_)
    {
        auto desc5 = meshDesc.m_Desc5;

        auto meshX = GetMeshX(desc5.m_Path, desc5.m_Index);
        if (meshX)
        {
            auto mesh = meshX->m_Meshes[desc5.m_Type];
            return mesh;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return std::make_shared<Ogl::Gut::Mesh>(meshDesc);
    }

}
std::shared_ptr<Ogl::Gut::Material> Ogl::Gut::AssertManager::GetMaterial(const Ogl::Gut::Material::Desc &desc)
{
    auto scene = GetScene(desc.m_Path);
    if(scene)
    {
        if(scene->m_Materials.size()>desc.m_Index)
        {
            return scene->m_Materials[desc.m_Index];
        }
    }
    return nullptr;
};

std::string Ogl::Gut::AssertManager::GetText(const std::string& path)
{   
    if (fs::exists(path))
    {
        std::string ctx = Ogl::Gut::Util::readFile(path);
        m_Texts[path] = ctx;

        return ctx;
    }
    return "";
}

std::shared_ptr<Ogl::Gut::Scene> Ogl::Gut::AssertManager::GetScene(const std::string& path)
{
    if (!LoadScene(path))
    {
        std::cout << "cannot load scene  " << path << std::endl;
    }

    return m_Scenes[path];
}

std::shared_ptr<Ogl::Gut::Channel> Ogl::Gut::AssertManager::GetNodeAnim(const Ogl::Gut::Channel::Desc &desc)
{
    auto anim = GetAnimation(desc.m_Path, desc.m_AnimIndex);
    
    if(anim)
    {
        if(anim->HasChannel(desc.m_NodeName))
        {
            return anim->m_Channels[desc.m_NodeName];
        };
    };
    return nullptr;
};

std::shared_ptr<Ogl::Gut::Animation> Ogl::Gut::AssertManager::GetAnimation(const AnimationDesc1& desc)
{
    return GetAnimation(desc.m_Path, desc.m_Index);
}

std::shared_ptr<Ogl::Gut::Animation> Ogl::Gut::AssertManager::GetAnimation(const AnimationDesc &desc)
{
    return GetAnimation(desc.m_Desc1);
}



AssertManager* Ogl::Gut::AssertManager::Inst()
{
    static AssertManager inst;
    return &inst;
}