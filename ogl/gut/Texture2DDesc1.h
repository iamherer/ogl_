#pragma once


#include "ogl/gut/Type.h"
#include "ogl/gut/Image.h"


namespace Ogl
{
	namespace Gut
	{
		struct Texture2DDesc1 :public Base
		{
			int m_Mipmap;
			Ogl::Gut::SamplerType m_SamplerType;
			Ogl::Gut::Image m_Image;

			void Write(const std::string& path);

			void Write(std::ofstream& ofs);

			void Read(const std::string& path);

			void Read(std::ifstream& ifs);

		};
	};
}