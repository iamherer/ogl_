#pragma once

#include "ogl/gut/pch.h"
#include "ogl/gut/Buffer.h"

namespace Ogl
{
	namespace Gut
	{

		struct VertexBuffer :public Ogl::Gut::Buffer
		{
			VertexBuffer()
				:Ogl::Gut::Buffer(GL_ARRAY_BUFFER)
			{

			}
			static void UnBind()
			{
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			}
		};
	}
}
