#pragma once

#include "ogl/gut/pch.h"
#include "ogl/gut/Base.h"

namespace Ogl
{
	namespace Gut
	{ 
        struct ShaderDesc2 :public Base
        {
            std::string m_VertFile;
            std::string m_FragFile;
        };
    }
}
