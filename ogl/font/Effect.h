#pragma once

#include "ogl/gut/EffectBase.h"
#include "ogl/gut/Shader.h"
#include "ogl/gut/Mesh.h"
#include "ogl/font/FontLib.h"
#include "ogl/font/TextLine.h"


namespace Ogl
{
	namespace Font
	{
		struct Effect :public Ogl::Gut::EffectBase
		{
			std::shared_ptr<Ogl::Gut::Shader> m_Shader;
			std::unordered_map<std::string, std::shared_ptr<FontLib>> m_FontLibs;

			Effect();
			Effect(const std::string& fragCode);
			void Begin();
			void Viewport(const Ogl::Math::Rect& rect);
			void RenderTextLine(const TextLine& inst);
			void End();
			void InitRenderDesc();

			bool RegisterFontFile(const std::string& path);
			bool RegisterFontFile(const std::string& path, const std::string& key);
			bool HasFontFamily(const std::string& fontFamily);
			std::vector<std::string> GetFamilys();
			std::string m_FragMain;
			std::string GetFragCode();
			static std::string VertCode();
			static std::string DefaultragCode();
			static std::string FragTemp();
		};
	}
};