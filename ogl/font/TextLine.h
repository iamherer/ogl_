#pragma once


#include "ogl/font/pch.h"

namespace Ogl
{
	namespace Font
	{


		struct TextLine {

			//char m_Text[128];
			std::string m_Text;
			glm::vec2 m_Pos;
			int m_FontSize;
			glm::vec3 m_TextColor;
			std::string m_FontFamily;
			TextLine()
				:m_Pos(glm::vec2(0.0f)), m_FontSize(17), m_TextColor(glm::vec3(0.0f))
			{

			}
		};
	}
}