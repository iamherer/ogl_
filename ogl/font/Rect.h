#pragma once

#include "glm/glm.hpp"

namespace Ogl
{
    namespace Font
    {

        struct Rect
        {
            glm::vec2 m_Pos;
            glm::vec2 m_Size;
        };
    };
};