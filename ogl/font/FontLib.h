#pragma once
#include "ogl/font/Character.h"
#include "ogl/font/VertexFont.h"
#include "ogl/gut/types.h"
#include "ogl/gut/RenderStatus.h"
#include "ogl/gut/EffectBase.h"
#include "ogl/math/Rect.h"

#include <string>
#include <codecvt>
#include <locale>

namespace Ogl
{
    namespace Font
    {
        struct FontLib 
        {

            std::string m_Path;

            FT_Library m_Lib;
            FT_Face m_Face;
            std::string m_Family;

            FontLib(const std::string& path);
            std::shared_ptr<Ogl::Gut::Mesh> fontMesh;

            std::map<int , std::map<wchar_t, Ogl::Font::Character>> m_CharMap;

            bool Create();

            void DoneFace();

            void DoneFreetype();

            void Done();

            bool HasWChar(const wchar_t& c, int fontSize);

            bool InitResources();

            bool LoadWChar(const wchar_t &c, int fontSize);

            bool LoadWstring(const std::wstring& str, int fontSize);

            void RenderTextMesh(const std::wstring &text,int fontSize,  float x, float y, float scale);

            void Viewport(const Ogl::Math::Rect& rect);

            void RenderWstring(const std::wstring& text, int fontSize, glm::vec2 pos,  glm::vec3 color);

            ~FontLib();

            static std::wstring UTF8ToWString(const std::string& utf8Str) {
                std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
                return converter.from_bytes(utf8Str);
            }

        private:
            // 3D Render Text

        };
    };
};
