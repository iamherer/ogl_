#pragma once

#include "ogl/font/pch.h"
#include "ogl/gut/Texture2D.h"

namespace Ogl
{
    namespace Font
    {
        struct Character
        {
            Ref<Ogl::Gut::Texture2D> m_Texture;
            glm::ivec2 m_Size;
            glm::ivec2 m_Bearing;
            unsigned int m_Advance;
        };
    };
};
