#pragma once

#include <iostream>
#include <map>
#include <unordered_map>
#include <fstream>
#include <ostream>
#include <assert.h>
#include <istream>
#include <memory>
#include <istream>
#include <vector>
#include <map>
#include <string>

#include <glm/glm.hpp>

// FreeType
#include "ft2build.h"
#include FT_FREETYPE_H

namespace Ogl
{
    namespace Font
    {

        template <typename T>
        using Ref = std::shared_ptr<T>;

        template <typename T, typename... Args>
        Ref<T> MakeRef(Args &&...args)
        {
            return std::make_shared<T>(std::forward<Args>(args)...);
        }

        template <typename T>
        using Unique = std::unique_ptr<T>;

        template <typename T, typename... Args>
        Unique<T> MakeUnique(Args &&...args)
        {
            return std::make_unique<T>(std::forward<Args>(args)...);
        }
    };
};
