#pragma once

#include "ogl/math/pch.h"

namespace Ogl
{
	namespace Math
	{
		struct Viewport
		{
			float left;
			float right;
			float bottom;
			float top;
			float zNear;
			float zFar;
			glm::mat4 GetOrthoMatrix() const;
		};
	};
};