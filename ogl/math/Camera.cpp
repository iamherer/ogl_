#include "ogl/math/Camera.h"

using namespace Ogl::Math;

void Camera::SetAspect(const float &aspect)
{
    this->aspect = aspect;
}

glm::mat4 Ogl::Math::Camera::GetProjViewMatrix() const
{
    return GetProjection() * GetViewMatrix();
}

