#include "ogl/math/OrthoCamera.h"

glm::mat4 Ogl::Math::OrthoCamera::GetProjViewMatrix()
{
	return GetOrthoMatrix() * GetViewMatrix();
}
