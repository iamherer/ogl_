#pragma once

#include "ogl/math/pch.h"


namespace Ogl
{
    namespace Math
    {
        struct SkinnedMeshData {

            std::vector<glm::ivec4> bones1s;
            std::vector<glm::vec4> weights1s;
            std::vector<glm::ivec4> bones2s;
            std::vector<glm::vec4> weights2s;
            void Resize(int numVertices);
            bool AddBoneWeight(int vertexId, int boneId, float weightValue);
        };
    };
};
