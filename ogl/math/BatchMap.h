#pragma once

#include "ogl/gut/pch.h"
#include "ogl/math/Geometry.h"


namespace Ogl
{
	namespace Math
	{

		struct BatchMap
		{
			std::string m_Name;
			GeometryData m_MeshData;

			BatchMap(const std::string& name, const GeometryData& meshData) :m_Name(name), m_MeshData(meshData)
			{

			}

			static GeometryData SubMeshData(const std::vector<Ogl::Math::BatchMap>& batchMaps)
			{
				std::vector<GeometryData> meshDatas;

				for (const Ogl::Math::BatchMap& batch : batchMaps)
				{
					meshDatas.push_back(batch.m_MeshData);
				}
				GeometryData subData = GeometryData::SubMeshData32(meshDatas);
				return subData;
			}
		};
	}
}