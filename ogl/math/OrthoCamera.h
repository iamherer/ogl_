#pragma once

#include "ogl/math/Transform.h"
#include "ogl/math/Viewport.h"

namespace Ogl
{
	namespace Math
	{
			struct OrthoCamera : public Transform, public Viewport
			{

				glm::mat4 GetProjViewMatrix();
			};
		};
	};