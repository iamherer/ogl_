#include "ogl/math/SkinnedMeshData.h"

void Ogl::Math::SkinnedMeshData::Resize(int numVertices)
{
	bones1s.resize(numVertices, glm::ivec4(-1));
	weights1s.resize(numVertices, glm::vec4(0.0f));
	bones2s.resize(numVertices, glm::ivec4(-1));
	weights2s.resize(numVertices, glm::vec4(0.0f));
}

bool Ogl::Math::SkinnedMeshData::AddBoneWeight(int vertexId, int boneId, float weightValue)
{
	glm::ivec4& aBones1 = bones1s[vertexId];
	glm::vec4& aWeights1 = weights1s[vertexId];
	glm::ivec4& aBones2 = bones2s[vertexId];
	glm::vec4& aWeights2 = weights2s[vertexId];

	for (int i = 0; i < 4; i++)
	{
		if (aWeights1[i] == 0.0f)
		{
			aWeights1[i] = weightValue;
			aBones1[i] = boneId;
			return true;
		}
	}

	for (int i = 0; i < 4; i++)
	{
		if (aWeights2[i] == 0.0f)
		{
			aWeights2[i] = weightValue;
			aBones2[i] = boneId;
			return true;
		}
	}
	return false;

}
