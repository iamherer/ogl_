#pragma once
#include "ogl/math/pch.h"
#include "ogl/math/Transform.h"
#include "ogl/math/Frustum.h"

namespace Ogl
{
	namespace Math
	{

		struct Camera : public Transform, public Frustum
		{
			void SetAspect(const float &aspect);

			glm::mat4 GetProjViewMatrix() const;
		};
	}
}
