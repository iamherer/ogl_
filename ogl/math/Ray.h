#pragma once

#include "ogl/math/pch.h"

namespace Ogl
{
	namespace Math
	{
		struct Ray
		{
			glm::vec3 origin = glm::vec3(0.0f);
			glm::vec3 direction = glm::vec3(1.0f);
		};
	};
};