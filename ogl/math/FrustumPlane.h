#pragma once

#include "ogl/math/Plane.h"
#include "ogl/math/Transform.h"
#include "ogl/math/Viewport.h"
#include "ogl/math/Frustum.h"

namespace Ogl
{
    namespace Math
    {
        struct FrustumPlane
        {
            Plane topFace;
            Plane bottomFace;

            Plane rightFace;
            Plane leftFace;

            Plane farFace;
            Plane nearFace;

            bool Create(const Transform &transform, const Frustum &frustum);
            bool Create(const Transform &transform, const Viewport &vp);
        };
    };
};