#include "ogl/math/GeometryData.h"

bool GeometryData::ResizeHeader(const Header& header)
{
	vertices.resize(header.numVertcies);
    normals.resize(header.numNormals);
    texcoords.resize(header.numTexCoords);
    tangents.resize(header.numTangents);
    bigTangents.resize(header.numBigTangents);
	
    return true;
}

void GeometryData::Write(std::ofstream& ofs)
{
    Header header = GetHeader();

    ofs.write((const char*)&header, sizeof(header));
    ofs.write((const char*)&vertices[0], sizeof(glm::vec3) * vertices.size());
    ofs.write((const char*)&normals[0], sizeof(glm::vec3) * normals.size());
    ofs.write((const char*)&texcoords[0], sizeof(glm::vec2) * texcoords.size());
    ofs.write((const char*)&tangents[0], sizeof(glm::vec3) * tangents.size());
    ofs.write((const char*)&bigTangents[0], sizeof(glm::vec3) * bigTangents.size());



}
