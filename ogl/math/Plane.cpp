#include "ogl/math/Plane.h"

void Ogl::Math::Plane::Create(const Transform &transform)
{
    normal = transform.GetUp();
    float D = -glm::dot(transform.position, normal);
    distance = -D;
}