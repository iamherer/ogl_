#pragma once


#include "ogl/math/pch.h"

namespace Ogl
{
	namespace Math
	{ 
		struct Rect {

			glm::vec2 m_Pos;
			glm::vec2 m_Size;
			Rect() = default;
			Rect(const glm::vec2& pos, const glm::vec2& sz) :m_Pos(pos), m_Size(sz)
			{

			}
			glm::mat4 GetProj() const
			{
				return glm::ortho(m_Pos.x, m_Pos.x + m_Size.x, m_Pos.y, m_Pos.y + m_Size.y);
			}
			glm::vec2 RightBottom() const
			{
				return m_Pos + m_Size;
			}
			glm::vec2 RightUp()const
			{
				return m_Pos + glm::vec2(m_Size.x, 0.0f);
			}
			glm::vec2 LeftDown() const
			{
				return m_Pos + glm::vec2(0.0f ,m_Size.y);
			}
			glm::vec2 LeftTop() const
			{
				return m_Pos;
			}
			glm::vec2 Size() const
			{
				return m_Size;
			}
			float GetArea() const
			{
				return m_Size.x * m_Size.y;
			}
		};

	}
}