#pragma once

#include "ogl/math/pch.h"
#include "ogl/math/Plane.h"

namespace Ogl
{
    namespace Math
    {
        struct Util
        {
            Plane CreatePlaneFrom(const Transform &t);

            bool RayCast(const Plane &plane, const Ray &ray, glm::vec3 &pos);

            glm::mat4 Mirror(const Plane &plane, const glm::mat4 &model);
        
        };
    };
};