#include "ogl/math/Viewport.h"

using namespace Ogl::Math;

glm::mat4 Viewport::GetOrthoMatrix() const
{
	return glm::orthoRH(left, right, bottom, top, zNear, zFar);
}
