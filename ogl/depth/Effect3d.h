#pragma once


#include "ogl/gut/pch.h"
#include "ogl/gut/Effect3d.h"


namespace Ogl
{
	namespace Depth
	{
		struct Effect3d :public Ogl::Gut::Effect3d {

			Effect3d();

			static const std::string GetFragCode();
		};
	}
}
