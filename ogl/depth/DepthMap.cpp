#include "ogl/depth/DepthMap.h"

using namespace Ogl::depth;

DepthMap::DepthMap(int width, int height)
{
	m_FrameBuffer = std::make_shared<Ogl::Gut::FrameBuffer>();
	m_DepthTexture = std::make_shared<Ogl::Gut::Texture2D>();

	m_FrameBuffer->Bind();

	{
		texDesc.m_Width = width;
		texDesc.m_Height = height;
		texDesc.m_Format = GL_DEPTH_COMPONENT;
		texDesc.m_InternalFormat = GL_DEPTH_COMPONENT;
		texDesc.m_DataType = GL_FLOAT;
		texDesc.m_Data = 0;
		m_DepthTexture->Bind();
		m_DepthTexture->TexImage2D(GL_TEXTURE_2D, &texDesc);
	}
	{
		Ogl::Gut::SamplerDesc samDesc;
		samDesc.minFilter = GL_NEAREST;
		samDesc.magFilter = GL_NEAREST;
		samDesc.wrapS = GL_REPEAT;
		samDesc.wrapT = GL_REPEAT;
		samDesc.useBorderColor = true;
		samDesc.borderColor[0] = 1.0f;
		samDesc.borderColor[1] = 1.0f;
		samDesc.borderColor[2] = 1.0f;
		samDesc.borderColor[3] = 1.0f;
		m_DepthTexture->Bind();
		m_DepthTexture->m_SampDesc = samDesc;
		m_DepthTexture->SetSampler();

	}
	m_FrameBuffer->BufferTexture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_DepthTexture->m_Id);
	m_FrameBuffer->DrawBuffer(GL_NONE);
	m_FrameBuffer->ReadBuffer(GL_NONE);

	Ogl::Gut::FrameBuffer::UnBind();


}

void DepthMap::Begin()
{
	m_FrameBuffer->Bind();
	glViewport(0, 0, texDesc.m_Width, texDesc.m_Height);
}

void DepthMap::End()
{
	Ogl::Gut::FrameBuffer::UnBind();

}
