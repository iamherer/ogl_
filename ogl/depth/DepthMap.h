#pragma once
#include "ogl/gut/Framebuffer.h"
#include "ogl/gut/Texture2D.h"

namespace Ogl
{
	namespace depth
	{

		struct DepthMap {

			std::shared_ptr<Ogl::Gut::FrameBuffer> m_FrameBuffer;
			std::shared_ptr<Ogl::Gut::Texture2D> m_DepthTexture;
			DepthMap(int width, int height);

			Ogl::Gut::Image texDesc;
			void Begin();

			void End();

		};
	}
};