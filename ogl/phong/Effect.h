#pragma once


#include "ogl/gut/Effect3d.h"
#include "ogl/gut/types.h"
#include "ogl/phong/LightEffect.h"

namespace Ogl
{
	namespace Phong
	{
		struct Effect :public Ogl::Gut::Effect3d {

			std::shared_ptr<Ogl::Gut::UniformBuffer> m_lightBuffer, m_MatBuffer;

			Effect();
			void Begin();
			void SetLightEffect(const Ogl::Phong::LightEffect& lightEffect);
			void RenderMesh(const Ogl::Gut::Mesh* mesh, const Ogl::Math::Transform& transform, const Ogl::Phong::Material& material);
			void RenderSkinnedMesh(const Ogl::Gut::Mesh* mesh, const Ogl::Math::Transform& transform, const Ogl::Gut::Effect3d::BoneData& boneData,const Ogl::Phong::Material& material);
			void End();


			static std::string GetFragMain();
		};
	};

};